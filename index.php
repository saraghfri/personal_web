<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Sara Ghaffari</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="Img/icon/Iconexpo-Speech-Balloon-Grey-Speech-balloon-white-s.ico" rel="icon" type="image/x-icon">
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
</head>
<body onLoad="typeWriter()">
<div class="container-fluid">
    <div>
        <img id="backpic" src="img/backpic.jpg" width="100%" height="100%" alt="Sara Ghaffari">
    </div>
    <div>
        <img id="roundpic" src="img/roundPic.png" width="800px" height="800px" alt="Sara Ghaffari"
             title="Sara Ghaffari">
    </div>

    <header class="row" id="name">
        <div class="col-sm-12 col-12">
            <h1 id="typist"></h1>
            <h2>Freelancer</h2>
        </div>
    </header>
    <nav class="row">
        <div class="col-sm-12 col-12">
            <ul>
                <a href="#">
                    <li>Home</li>
                </a>
            </ul>
        </div>
    </nav>
    <!--SlideShow-->
    <div class="slideshow-container d-none d-sm-none d-md-inline row">
        <div class="slide1 mySlides fade col-sm-12">
            <h1>About Me</h1>
            <div class="row">
                <div class="col-sm-6">I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just
                    click “Edit Text” or double click me to add your own content and make changes to the font. Feel free
                    to drag and drop me anywhere you like on your page. I’m a great place for you to tell a story and
                    let your users know a little more about you.I'm a paragraph. Click here to add your own text and
                    edit me. It’s easy. Just click “Edit Text” or double click me to add your own content and make
                    changes to the font. Feel free to drag and drop me anywhere you like on your page. I’m a great place
                    for you to tell a story and let your users know a little more about you.
                </div>
                <div class="col-sm-6">I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just
                    click “Edit Text” or double click me to add your own content and make changes to the font. Feel free
                    to drag and drop me anywhere you like on your page. I’m a great place for you to tell a story and
                    let your users know a little more about you.I'm a paragraph. Click here to add your own text and
                    edit me. It’s easy. Just click “Edit Text” or double click me to add your own content and make
                    changes to the font. Feel free to drag and drop me anywhere you like on your page. I’m a great place
                    for you to tell a story and let your users know a little more about you.
                </div>
            </div>
        </div>
        <div class="slide2 mySlides fade col-sm-12">
            <h1>Education</h1>
            <div class="row">
                <div class="col-sm-4"><h3>Sianat Highschool</h3>
                    <small>2011-2015</small>
                    <p>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text”
                        or double click me to add your own content and make changes to the font. Feel free to drag and
                        drop me anywhere you like on your page. I’m a great place for you to tell a story and let your
                        users know a little more about you</p></div>
                <div class="col-sm-4"><h3>Azad University</h3>
                    <small>2015</small>
                    <p>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text”
                        or double click me to add your own content and make changes to the font. Feel free to drag and
                        drop me anywhere you like on your page. I’m a great place for you to tell a story and let your
                        users know a little more about you.</p></div>
                <div class="col-sm-4"><h3>WebDesigning Class</h3>
                    <small>2016</small>
                    <p>I'm a paragraph. Click here to add your own text and edit me. It’s easy. Just click “Edit Text”
                        or double click me to add your own content and make changes to the font. Feel free to drag and
                        drop me anywhere you like on your page. I’m a great place for you to tell a story and let your
                        users know a little more about you.</p></div>
            </div>
        </div>
        <div class="slide3 mySlides fade col-sm-12">


        </div>
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <br>
        <div style="text-align:center;">
            <span class="dot" onclick="currentSlide(1)"></span>
            <span class="dot" onclick="currentSlide(2)"></span>
            <span class="dot" onclick="currentSlide(3)"></span>
        </div>
    </div>
    <!--SlideShow-->
    <section class="row">
        <div class="col-12 col-sm-6">


        </div>
    </section>
</div>

<footer class="row">
    <div class="col-sm-4">
        <div class="row">
            <div class="col-sm-6">
                <h3 id="phone">Phone</h3>
                <h3 id="email">Email</h3>
                <h3 id="address">Address</h3>
            </div>
            <div class="col-sm-6">
                <h4>+989128332015</h4>
                <h4>saraghfri96@yahoo.com</h4>
                <h4>Pirouzi Ave,Pourmand St,Goudarzi Alley,No.5</h4>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <a href="#"><i class="fa fa-gitlab fa-3x fa-fw"></i></a>
        <a href="#"><i class="fa fa-telegram fa-3x fa-fw"></i></a>
        <a href="#"><i class="fa fa-twitter fa-3x fa-fw"></i></a>
        <a href="#"><i class="fa fa-linkedin fa-3x fa-fw"></i></a>
    </div>
    <div class="col-sm-4">
        <div class="calendar">
                <?php
                $url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22tehran%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
                $weather = file_get_contents($url);
                $array = json_decode($weather, true);
                echo "<b>" . "Current Date : " . $array['query']['results']['channel']['item']['forecast']['0']['date'] . "</b>";
                ?>
        </div>
    </div>


    </div>
</footer>

<script src="bootstrap/js/jquery-3.2.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="script.js"></script>
</body>
</html>
